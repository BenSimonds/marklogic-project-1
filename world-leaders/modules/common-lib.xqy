xquery version "1.0-ml";
module namespace co = "http://madeup.com/fakeURI/world-leaders/common";

(:Declare Namespaces:)
declare namespace wl = "http://marklogic.com/mlu/world-leaders";

(: Declare Functions:)
declare function co:in-office() as xs:string{
  let $incount := fn:count(/wl:leader/wl:positions/wl:position[1]/wl:enddate/text()[. = "present"])
  return fn:concat(" (in office: ",  $incount, ")")
};