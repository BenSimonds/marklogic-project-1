xquery version "1.0-ml";
declare namespace ts="http://marklogic.com/MLU/top-songs";
import module namespace search = "http://marklogic.com/appservices/search" at "/MarkLogic/appservices/search/search.xqy";
import module namespace disp = "http://marklogic.com/MLU/top-songs/display" at "modules/display-lib.xqy";
import module namespace adv = "http://marklogic.com/MLU/top-songs/advanced" at "modules/advanced-lib.xqy";

declare variable $options := 
<options xmlns="http://marklogic.com/appservices/search">

<constraint name="creator">
  <word>
    <field name="creator"/>
  </word>
</constraint>
<constraint name="artist">
<range type="xs:string" collation="http://marklogic.com/collation/en/S1/AS/T00BB">
<element ns="http://marklogic.com/MLU/top-songs" name="artist"/>
<facet-option>limit=30</facet-option>
<facet-option>frequency-order</facet-option>
<facet-option>descending</facet-option>
</range>
</constraint> 
  <constraint name="genre">
    <range type="xs:string" collation="http://marklogic.com/collation/en/S1/AS/T00BB">
     <element ns="http://marklogic.com/MLU/top-songs" name="genre"/>
     <facet-option>limit=20</facet-option>
     <facet-option>frequency-order</facet-option>
     <facet-option>descending</facet-option>
    </range>
  </constraint> 
  <constraint name="decade">
    <range type="xs:date">
      <bucket ge="2010-01-01" name="2010s">2010s</bucket>
      <bucket lt="2010-01-01" ge="2000-01-01" name="2000s">2000s</bucket>
      <bucket lt="2000-01-01" ge="1990-01-01" name="1990s">1990s</bucket>
      <bucket lt="1990-01-01" ge="1980-01-01" name="1980s">1980s</bucket>
      <bucket lt="1980-01-01" ge="1970-01-01" name="1970s">1970s</bucket>
      <bucket lt="1970-01-01" ge="1960-01-01" name="1960s">1960s</bucket>
      <bucket lt="1960-01-01" ge="1950-01-01" name="1950s">1950s</bucket>
      <bucket lt="1950-01-01" name="1940s">1940s</bucket>
      <attribute ns="" name="last"/>
      <element ns="http://marklogic.com/MLU/top-songs" name="weeks"/>
      <facet-option>limit=10</facet-option>
    </range>
  </constraint>
<transform-results apply="snippet">
<preferred-elements>
<element ns="http://marklogic.com/MLU/top-songs" name="descr"/>
</preferred-elements>
</transform-results>
<search:operator name="sort">
<search:state name="relevance">
<search:sort-order>
<search:score/>
</search:sort-order>
</search:state>
<search:state name="newest">
<search:sort-order direction="descending" type="xs:date">
<search:attribute ns="" name="last"/>
<search:element ns="http://marklogic.com/MLU/top-songs" name="weeks"/>
</search:sort-order>
<search:sort-order>
<search:score/>
</search:sort-order>
</search:state>
<search:state name="oldest">
<search:sort-order direction="ascending" type="xs:date">
<search:attribute ns="" name="last"/>
<search:element ns="http://marklogic.com/MLU/top-songs" name="weeks"/>
</search:sort-order>
<search:sort-order>
<search:score/>
</search:sort-order>
</search:state>            
<search:state name="title">
<search:sort-order direction="ascending" type="xs:string">
<search:element ns="http://marklogic.com/MLU/top-songs" name="title"/>
</search:sort-order>
<search:sort-order>
<search:score/>
</search:sort-order>
</search:state>            
<search:state name="artist">
<search:sort-order direction="ascending" type="xs:string">
<search:element ns="http://marklogic.com/MLU/top-songs" name="artist"/>
</search:sort-order>
<search:sort-order>
<search:score/>
</search:sort-order>
</search:state>            
</search:operator>
</options>;

declare variable $q-text := 
  let $q := if(xdmp:get-request-field("advanced"))
            then adv:advanced-q()
            else xdmp:get-request-field("q", "sort:newest")
  let $q := local:add-sort($q)
  return $q;

declare variable $results := search:search($q-text, $options, xs:unsignedLong(xdmp:get-request-field("start","1")));

declare variable $facet-size as xs:integer := 8;



declare function local:birthday()
{
  let $bday := xdmp:get-request-field("bday")
  return if($bday castable as xs:date)
  then let $week := (for $i in //ts:week[. gt $bday]
    order by $i ascending
    return $i)[1]
  let $uri := fn:base-uri($week)
  return disp:display-song-detail($uri)
  else <div>Sorry, invalid date entered.</div>
};


declare function local:login-area()
{
if(xdmp:get-current-roles() eq xdmp:role("top-songs-admin"))
then
<div class="padleft">
<div class="purplesubheading">you are logged in as an administrator</div>
<a href="index.xqy?insert=true">insert new song</a>&#160;
<a href="logout.xqy">log out</a>
</div>
else
<div class="padleft">
<div class="purplesubheading">administrator login</div>
<form name="formlogin" method="post" action="login.xqy" id="formlogin">
<input type="text" name="username" id="username" size="15"/><br/>
<input type="password" name="password" id="password" size="10"/>
<input type="submit" id="btnlogin" value="go"/><br/>
<div class="tinynoitalics">(login/password)</div>
</form>
</div>
};

declare function local:new-song()
{
    <div>
      <form name="form1" method="get" action="index.xqy">
      <input type="hidden" name="save" value="true"/>
      <table border="0" cellspacing="8" xmlns="http://www.w3.org/1999/xhtml">
        <tr>
          <td align="right" valign="top">&#160;</td>
          <td class="songnamelarge"><span class="tiny">&#160;</span><br />
            insert new song<br />
            <span class="tiny">&#160;</span></td>
        </tr>
        <tr>
          <td align="right" valign="top"> song title:</td>
          <td><input type="text" name="title" id="title" size="50"/>
            <br />
            <span class="tiny">e.g. Imma Be</span></td>
        </tr>
        <tr>
          <td align="right" valign="top">artist:</td>
          <td><input type="text" name="artist" id="artist" size="50"/>
            <br />
            <span class="tiny">e.g. Black Eyed Peas</span></td>
        </tr>
        <tr>
          <td align="right" valign="top">album:</td>
          <td><input type="text" name="album" id="album" size="50"/>
            <span class="tiny"><br />
            e.g. The E.N.D.</span><br /></td>
        </tr>
        <tr>
          <td align="right" valign="top">genres:</td>
          <td><input type="text" name="genres" id="genres" size="50"/>
            <br />
            <span class="tiny">e.g. hip-hop, electro-hop</span></td>
        </tr>
        <tr>
          <td align="right" valign="top">writers:</td>
          <td><input type="text" name="writers" id="writers" size="50"/>
            <br />
            <span class="tiny">e.g. T. Brenneck, M. Deller, D. Foder, K. Harris, Allen Pineda</span></td>
        </tr>
        <tr>
          <td align="right" valign="top">producers:</td>
          <td><input type="text" name="producers" id="producers" size="50"/>
            <br />
            <span class="tiny">e.g. will.i.am</span> <br /></td>
        </tr>
        <tr>
          <td align="right" valign="top">label:</td>
          <td><input type="text" name="label" id="label" size="50"/>
            <br />
            <span class="tiny">e.g. Interscope</span><br /></td>
        </tr>
        <tr>
          <td align="right" valign="top">description:</td>
          <td><textarea name="description" id="description" cols="40"  rows="5"/>
          <br />
            <span class="tiny">e.g. "Imma Be" is a song performed by the American group...</span></td>
        </tr>
        <tr>
          <td align="right" valign="top">#1 week:</td>
          <td><input type="text" name="weeks" id="weeks" size="50"/>
            <br />
            <span class="tiny">e.g. 2010-03-06, 2010-03-13</span></td>
        </tr>
        <tr valign="top">
          <td align="right" valign="top">&#160;</td>
          <td><input type="submit" name="insertbtn" id="insertbtn" value="insert"/></td>
          </tr>
      </table>
      </form>
    </div>
};

declare function local:save-song()
{   
    let $title := xdmp:get-request-field("title")
    let $artist := xdmp:get-request-field("artist")
    let $album := xdmp:get-request-field("album")
    let $genres := xdmp:get-request-field("genres")
    let $writers := xdmp:get-request-field("writers")
    let $producers := xdmp:get-request-field("producers")
    let $label := xdmp:get-request-field("label")
    let $description := xdmp:get-request-field("description")   
    let $weeks := xdmp:get-request-field("weeks")
    let $song:= <top-song xmlns="http://marklogic.com/MLU/top-songs">  
                    <title>{$title}</title>
                    <artist>{$artist}</artist>
                    <album>{$album}</album>
                    <genres>
                    { 
                       for $genre in fn:tokenize($genres,",")
                       return <genre>{$genre}</genre>
                    }
                    </genres>
                    <writers>
                    { 
                       for $writer in fn:tokenize($writers,",")
                       return <writer>{$writer}</writer>
                    }
                    </writers>
                    <producers>
                    { 
                       for $producer in fn:tokenize($producers,",")
                       return <producer>{$producer}</producer>
                    }
                    </producers>
                    <label>{$label}</label>
                    <weeks last="{(fn:tokenize($weeks,","))[fn:last()]}">   
                    { 
                       for $week in fn:tokenize($weeks,",")
                       return <week>{$week}</week>
                    }                   
                    </weeks>                        
                    <descr>{$description}</descr>
                </top-song>
    return xdmp:document-insert(xdmp:diacritic-less(fn:replace(fn:concat("/songs/",$artist," ",$title,".xml")," ","-")), $song,
    (xdmp:permission("top-songs-user","read"), xdmp:permission("top-songs-admin","update"))),"Song loaded."
};

declare function local:result-controller()
{
  if(xdmp:get-request-field("save"))
  then local:save-song()
  else
    if(xdmp:get-request-field("insert"))
    then local:new-song()
    else
      if(xdmp:get-request-field("bday"))
      then local:birthday()
      else 	if(xdmp:get-request-field("uri"))
      then local:song-detail()
      else local:search-results()
};

(: gets the current sort argument from the query string :)
declare function local:get-sort($q){
  fn:replace(fn:tokenize($q," ")[fn:contains(.,"sort")],"[()]","")
};

(: adds sort to the search query string :)
declare function local:add-sort($q){
  let $sortby := local:sort-controller()
  return
  if($sortby)
  then
  let $old-sort := local:get-sort($q)
  let $q :=
  if($old-sort)
  then search:remove-constraint($q,$old-sort,$options)
  else $q
  return fn:concat($q," sort:",$sortby)
  else $q
};

(: determines if the end-user set the sort through the drop-down or through editing the search text field :)
declare function local:sort-controller(){
  if(xdmp:get-request-field("submitbtn") or not(xdmp:get-request-field("sortby")))
  then 
  let $order := fn:replace(fn:substring-after(fn:tokenize(xdmp:get-request-field("q","sort:newest")," ")[fn:contains(.,"sort")],"sort:"),"[()]","")
  return 
  if(fn:string-length($order) lt 1)
  then "relevance"
  else $order
  else xdmp:get-request-field("sortby")
};

(: builds the sort drop-down with appropriate option selected :)
declare function local:sort-options(){
  let $sortby := local:sort-controller()
  let $sort-options := 
  <options>
  <option value="relevance">relevance</option>   
  <option value="newest">newest</option>
  <option value="oldest">oldest</option>
  <option value="artist">artist</option>
  <option value="title">title</option>
  </options>
  let $newsortoptions := 
  for $option in $sort-options/*
  return 
  element {fn:node-name($option)}
  {
    $option/@*,
    if($sortby eq $option/@value)
    then attribute selected {"true"}
    else (),
    $option/node()
  }
  return 
  <div id="sortbydiv">
  sort by: 
  <select name="sortby" id="sortby" onchange='this.form.submit()'>
  {$newsortoptions}
  </select>
  </div>
};

declare function local:pagination($resultspag)
{
  let $start := xs:unsignedLong($resultspag/@start)
  let $length := xs:unsignedLong($resultspag/@page-length)
  let $total := xs:unsignedLong($resultspag/@total)
  let $last := xs:unsignedLong($start + $length -1)
  let $end := if ($total > $last) then $last else $total
  let $qtext := $resultspag/search:qtext[1]/text()
  let $next := if ($total > $last) then $last + 1 else ()
  let $previous := if (($start > 1) and ($start - $length > 0)) then fn:max((($start - $length),1)) else ()
  let $next-href := 
  if ($next) 
  then fn:concat("/index.xqy?q=",if ($qtext) then fn:encode-for-uri($qtext) else (),"&amp;start=",$next,"&amp;submitbtn=page")
  else ()
  let $previous-href := 
  if ($previous)
  then fn:concat("/index.xqy?q=",if ($qtext) then fn:encode-for-uri($qtext) else (),"&amp;start=",$previous,"&amp;submitbtn=page")
  else ()
  let $total-pages := fn:ceiling($total div $length)
  let $currpage := fn:ceiling($start div $length)
  let $pagemin := 
  fn:min(for $i in (1 to 4)
    where ($currpage - $i) > 0
    return $currpage - $i)
  let $rangestart := fn:max(($pagemin, 1))
  let $rangeend := fn:min(($total-pages,$rangestart + 4))

  return (
    <div id="countdiv"><b>{$start}</b> to <b>{$end}</b> of {$total}</div>,
    local:sort-options(),
    if($rangestart eq $rangeend)
    then ()
    else
    <div id="pagenumdiv"> 
    { if ($previous) then <a href="{$previous-href}" title="View previous {$length} results"><img src="images/prevarrow.gif" class="imgbaseline"  border="0" /></a> else () }
    {
     for $i in ($rangestart to $rangeend)
     let $page-start := (($length * $i) + 1) - $length
     let $page-href := concat("/index.xqy?q=",if ($qtext) then encode-for-uri($qtext) else (),"&amp;start=",$page-start,"&amp;submitbtn=page")
     return 
     if ($i eq $currpage) 
     then <b>&#160;<u>{$i}</u>&#160;</b>
     else <span class="hspace">&#160;<a href="{$page-href}">{$i}</a>&#160;</span>
   }
   { if ($next) then <a href="{$next-href}" title="View next {$length} results"><img src="images/nextarrow.gif" class="imgbaseline" border="0" /></a> else ()}
   </div>
   )
};

declare function local:search-results()
{
	
	let $items :=
  for $song in $results/search:result
  let $uri := fn:data($song/@uri)
  let $song-doc := fn:doc($uri)
  return 
  <div>
  <div class="songname">"{$song-doc//ts:title/text()}" by {$song-doc//ts:artist/text()}</div>
  <div class="week"> ending week: {fn:data($song-doc//ts:weeks/@last)} 
  (total weeks: {fn:count($song-doc//ts:weeks/ts:week)})</div>    
  {if ($song-doc//ts:genres/ts:genre) then <div class="genre">genre: {fn:lower-case(fn:string-join(($song-doc//ts:genres/ts:genre),", "))}</div> else ()}
  <div class="description">{disp:description($song)}&#160;
  <a href="index.xqy?uri={xdmp:url-encode($uri)}">[more]</a>
  </div>
  </div>
  return
  if ($items)
  then (local:pagination($results), $items) 
  else <div>Sorry, no results for your search.<br/><br/><br/></div>
};

declare function local:song-detail()
{
	let $uri := xdmp:get-request-field("uri")
	return disp:display-song-detail($uri)
};

declare function local:facets()
{
  for $facet in $results/search:facet
  let $facet-count := fn:count($facet/search:facet-value)
  let $facet-name := fn:data($facet/@name)
  return
  if($facet-count > 0)
  then <div class="facet">
  <div class="purplesubheading"><img src="images/checkblank.gif"/>{$facet-name}</div>
  {
    let $facet-items:=
      for $val in $facet/search:facet-value
      let $print := if($val/text()) then $val/text() else "Unknown"
      let $qtext := ($results/search:qtext)
      let $sort := local:get-sort($qtext)
      let $this :=
      if (fn:matches($val/@name/string(),"\W"))
      then fn:concat('"',$val/@name/string(),'"')
      else if ($val/@name eq "") then '""'
      else $val/@name/string()
      let $this := fn:concat($facet/@name,':',$this)
      let $selected := fn:matches($qtext,$this,"i")
      let $icon := 
      if($selected)
      then <img src="images/checkmark.gif"/>
      else <img src="images/checkblank.gif"/>
      let $link := 
      if($selected)
      then search:remove-constraint($qtext,$this,$options)
      else if(fn:string-length($qtext) gt 0)
      then fn:concat("(",$qtext,")"," AND ",$this)
      else $this
      let $link := if($sort and fn:not(local:get-sort($link))) then fn:concat($link," ",$sort) else $link
      let $link := fn:encode-for-uri($link)
      return
      <div class="facet-value">{$icon}<a href="index.xqy?q={$link}">
      {fn:lower-case($print)}</a> [{fn:data($val/@count)}]</div>
    return (
        <div>{$facet-items[1 to $facet-size]}</div>,
        if ($facet-count gt $facet-size)
        then (
            <div class="facet-hidden" id="{$facet-name}">{$facet-items[position() gt $facet-size]}</div>,
            <div class="facet-toggle" id="{$facet-name}_more"><img src="images/checkblank.gif"/><a href="javascript:toggle('{$facet-name}');" class="white">more...</a></div>,
            <div class="facet-toggle-hidden" id="{$facet-name}_less"><img src="images/checkblank.gif"/><a href="javascript:toggle('{$facet-name}');" class="white">less...</a></div>
        )
        else ()  
      )  
  }          
  </div>
  else <div>&#160;</div>
};

xdmp:set-response-content-type("text/html; charset=utf-8"),
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">',
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Top Songs</title>
<link href="css/top-songs.css" rel="stylesheet" type="text/css"/>
<script src="js/top-songs.js" type="text/javascript"/>
</head>
<body>
<div id="wrapper">
<div id="header"><a href="index.xqy"><img src="images/banner.jpg" width="918" height="153" border="0"/></a></div>
<div id="leftcol">
<img src="images/checkblank.gif"/>{local:facets()}
<br />
<div class="purplesubheading"><img src="images/checkblank.gif"/>check your birthday!</div>
<form name="formbday" method="get" action="index.xqy" id="formbday">
<img src="images/checkblank.gif" width="7"/>
<input type="text" name="bday" id="bday" size="15"/> 
<input type="submit" id="btnbday" value="go"/>
</form>
<div class="tinynoitalics"><img src="images/checkblank.gif"/>(e.g. 1965-10-31)</div>

<br/>
{local:login-area()}
<br/>
</div>
<div id="rightcol">
<form name="form1" method="get" action="index.xqy" id="form1">
<div id="searchdiv">
<input type="text" name="q" id="q" size="55" value="{$q-text}"/><button type="button" id="reset_button" onclick="document.getElementById('bday').value = ''; document.getElementById('q').value = ''; document.location.href='index.xqy'">x</button>&#160;
<input style="border:0; width:0; height:0; background-color: #A7C030" type="text" size="0" maxlength="0"/><input type="submit" id="submitbtn" name="submitbtn" value="search"/>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<a href="advanced.xqy">advanced search</a>
</div>
<div id="detaildiv">
{  local:result-controller()  }  	
</div>
</form>
</div>
<div id="footer"></div>
</div>
</body>
</html>
