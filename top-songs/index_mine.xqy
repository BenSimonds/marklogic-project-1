xquery version "1.0-ml";
declare namespace ts="http://marklogic.com/MLU/top-songs";
import module namespace search = "http://marklogic.com/appservices/search" at "/MarkLogic/appservices/search/search.xqy";

declare variable $options := 
	<options xmlns="http://marklogic.com/appservices/search">
	  <transform-results apply="snippet">
	    <preferred-elements>
	      <element ns="http://marklogic.com/MLU/top-songs" name="descr"/>
	    </preferred-elements>
	  </transform-results>
	  <search:operator name="sort">
	        <search:state name="relevance">
	            <search:sort-order>
	                <search:score/>
	            </search:sort-order>
	        </search:state>
	        <search:state name="newest">
	            <search:sort-order direction="descending" type="xs:date">
	                <search:attribute ns="" name="last"/>
	                <search:element ns="http://marklogic.com/MLU/top-songs" name="weeks"/>
	            </search:sort-order>
	            <search:sort-order>
	                <search:score/>
	            </search:sort-order>
	        </search:state>
	        <search:state name="oldest">
	            <search:sort-order direction="ascending" type="xs:date">
	                <search:attribute ns="" name="last"/>
	                <search:element ns="http://marklogic.com/MLU/top-songs" name="weeks"/>
	            </search:sort-order>
	            <search:sort-order>
	                <search:score/>
	            </search:sort-order>
	        </search:state>            
	        <search:state name="title">
	            <search:sort-order direction="ascending" type="xs:string">
	                <search:element ns="http://marklogic.com/MLU/top-songs" name="title"/>
	            </search:sort-order>
	            <search:sort-order>
	                <search:score/>
	            </search:sort-order>
	        </search:state>            
	        <search:state name="artist">
	            <search:sort-order direction="ascending" type="xs:string">
	                <search:element ns="http://marklogic.com/MLU/top-songs" name="artist"/>
	            </search:sort-order>
	            <search:sort-order>
	                <search:score/>
	            </search:sort-order>
	        </search:state>            
	  </search:operator>
	</options>;



declare function local:result-controller()
{
	if(xdmp:get-request-field("bday"))
	then local:birthday()
	else if(xdmp:get-request-field("q"))
	then local:search-results()
	else
	if(xdmp:get-request-field("uri"))
	then local:song-detail()
	else local:default-results()
};

declare function local:birthday()
{
    let $bday := xdmp:get-request-field("bday")
    return if($bday castable as xs:date)
           then let $week := (for $i in //ts:week[. gt $bday]
                                order by $i ascending
                                return $i)[1]
                return <div class="songnamelarge">{/ts:top-song[.//ts:week = $week]//ts:title/text()} by {/ts:top-song[.//ts:week = $week]//ts:artist/text()}</div>
           else <div>Sorry, invalid date entered.</div>
};

(: gets the current sort argument from the query string :)
declare function local:get-sort($q){
    fn:replace(fn:tokenize($q," ")[fn:contains(.,"sort")],"[()]","")
};

(: adds sort to the search query string :)
declare function local:add-sort($q){
    let $sortby := local:sort-controller()
    return
        if($sortby)
        then
            let $old-sort := local:get-sort($q)
            let $q :=
                if($old-sort)
                then search:remove-constraint($q,$old-sort,$options)
                else $q
            return fn:concat($q," sort:",$sortby)
        else $q
};

(: determines if the end-user set the sort through the drop-down or through editing the search text field :)
declare function local:sort-controller(){
    if(xdmp:get-request-field("submitbtn") or not(xdmp:get-request-field("sortby")))
    then 
        let $order := fn:replace(fn:substring-after(fn:tokenize(xdmp:get-request-field("q","sort:newest")," ")[fn:contains(.,"sort")],"sort:"),"[()]","")
        return 
            if(fn:string-length($order) lt 1)
            then "relevance"
            else $order
    else xdmp:get-request-field("sortby")
};

(: builds the sort drop-down with appropriate option selected :)
declare function local:sort-options(){
    let $sortby := local:sort-controller()
    let $sort-options := 
            <options>
                <option value="relevance">relevance</option>   
                <option value="newest">newest</option>
                <option value="oldest">oldest</option>
                <option value="artist">artist</option>
                <option value="title">title</option>
            </options>
    let $newsortoptions := 
        for $option in $sort-options/*
        return 
            element {fn:node-name($option)}
            {
                $option/@*,
                if($sortby eq $option/@value)
                then attribute selected {"true"}
                else (),
                $option/node()
            }
    return 
        <div id="sortbydiv">
             sort by: 
                <select name="sortby" id="sortby" onchange='this.form.submit()'>
                     {$newsortoptions}
                </select>
        </div>
};

declare function local:search-results()
{
    let $q := local:add-sort(xdmp:get-request-field("q"))
    let $results :=
        for $song in search:search($q, $options)/search:result
        let $uri := fn:data($song/@uri)
        let $song-doc := fn:doc($uri)
        return 
          <div>
             <div class="songname">"{$song-doc//ts:title/text()}" by {$song-doc//ts:artist/text()}</div>
             <div class="week"> ending week: {fn:data($song-doc//ts:weeks/@last)} 
                (total weeks: {fn:count($song-doc//ts:weeks/ts:week)})</div>    
             {if ($song-doc//ts:genres/ts:genre) then <div class="genre">genre: {fn:lower-case(fn:string-join(($song-doc//ts:genres/ts:genre),", "))}</div> else ()}
             <div class="description">{local:description($song)}&#160;
                <a href="index.xqy?uri={xdmp:url-encode($uri)}">[more]</a>
             </div>
          </div>
    return
        if ($results)
        then (local:sort-options(),$results)
        else <div>Sorry, no results for your search.<br/><br/><br/></div>
};

declare function local:description($song)
{
    for $text in $song/search:snippet/search:match/node() 
    return
    	if(fn:node-name($text) eq xs:QName("search:highlight"))
    	then <span class="highlight">{$text/text()}</span>
    	else $text
};


declare function local:default-results()
{
	(for $song in /ts:top-song 		 
		order by $song//ts:weeks/@last descending
		return (<div>
			<div class="songname">"{$song//ts:title/text()}" by {$song//ts:artist/text()}</div>
			<div class="week"> ending week: {fn:data($song//ts:weeks/@last)} 
			(total weeks: {fn:count($song//ts:weeks/ts:week)})</div>	
			<div class="genre">genre: 						  {fn:lower-case(fn:string-join(($song//ts:genres/ts:genre),", "))}</div>
			<div class="description">{fn:tokenize($song//ts:descr, " ") [1 to 70]} ...&#160;
			<a href="index.xqy?uri={xdmp:url-encode(fn:base-uri($song))}">[more]</a>
			</div>
			</div>)	   	
		)[1 to 10]
};

declare function local:song-detail()
{
	let $uri := xdmp:get-request-field("uri")
	let $song := fn:doc($uri) 
	return <div>
	<div class="songnamelarge">"{$song/ts:top-song/ts:title/text()}"</div>
	{if ($song/ts:top-song/ts:album/@uri) then <div class="albumimage"><img src="get-file.xqy?uri={xdmp:url-encode($song/ts:top-song/ts:album/@uri)}"/></div> else ()}
	<div class="detailitem">#1 weeks: {fn:count($song/ts:top-song/ts:weeks/ts:week)}</div>	
	<div class="detailitem">weeks: {fn:string-join(($song/ts:top-song/ts:weeks/ts:week), ", ")}</div>	
	{if ($song/ts:top-song/ts:genres/ts:genre) then <div class="detailitem">genre: {fn:lower-case(fn:string-join(($song/ts:top-song/ts:genres/ts:genre), ", "))}</div> else ()}
	{if ($song/ts:top-song/ts:artist/text()) then <div class="detailitem">artist: {$song/ts:top-song/ts:artist/text()}</div> else ()}
	{if ($song/ts:top-song/ts:album/text()) then <div class="detailitem">album: {$song/ts:top-song/ts:album/text()}</div> else ()}
	{if ($song/ts:top-song/ts:writers/ts:writer) then <div class="detailitem">writers: {fn:string-join(($song/ts:top-song/ts:writers/ts:writer), ", ")}</div> else ()}
	{if ($song/ts:top-song/ts:producers/ts:producer) then <div class="detailitem">producers: {fn:string-join(($song/ts:top-song/ts:producers/ts:producer), ", ")}</div> else ()}
	{if ($song/ts:top-song/ts:label) then <div class="detailitem">label: {$song/ts:top-song/ts:label}</div> else ()}
	{if ($song/ts:top-song/ts:formats/ts:format) then <div class="detailitem">formats: {fn:string-join(($song/ts:top-song/ts:formats/ts:format), ", ")}</div> else ()} 
	{if ($song/ts:top-song/ts:lengths/ts:length) then <div class="detailitem">lengths: {fn:string-join(($song/ts:top-song/ts:lengths/ts:length), ", ")}</div> else ()}
	{if ($song/ts:top-song/ts:descr) then <div class="detailitem">{$song/ts:top-song/ts:descr}</div> else ()}
	</div>
};



xdmp:set-response-content-type("text/html; charset=utf-8"),
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">',
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Top Songs</title>
<link href="css/top-songs.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="wrapper">
<div id="header"><a href="index.xqy"><img src="images/banner.jpg" width="918" height="153" border="0"/></a></div>
<div id="leftcol">
<img src="images/checkblank.gif"/>facet content here<br />
<br />
<div class="purplesubheading"><img src="images/checkblank.gif"/>check your birthday!</div>
<form name="formbday" method="get" action="index.xqy" id="formbday">
<img src="images/checkblank.gif" width="7"/>
<input type="text" name="bday" id="bday" size="15"/> 
<input type="submit" id="btnbday" value="go"/>
</form>
<div class="tinynoitalics"><img src="images/checkblank.gif"/>(e.g. 1965-10-31)</div>
</div>
<div id="rightcol">
<form name="form1" method="get" action="index.xqy" id="form1">
<div id="searchdiv">
<input type="text" name="q" id="q" size="50" value="{local:add-sort(xdmp:get-request-field("q"))}"/><button type="button" id="reset_button" onclick="document.getElementById('bday').value = ''; document.getElementById('q').value = ''; document.location.href='index.xqy'">x</button>&#160;
<input style="border:0; width:0; height:0; background-color: #A7C030" type="text" size="0" maxlength="0"/><input type="submit" id="submitbtn" name="submitbtn" value="search"/>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<a href="advanced.xqy">advanced search</a>
</div>
<div id="detaildiv">
{  local:result-controller()  }  	
</div>
</form>
</div>
<div id="footer"></div>
</div>
</body>
</html>
