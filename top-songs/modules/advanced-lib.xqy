module namespace adv = 
"http://marklogic.com/MLU/top-songs/advanced";

declare function advanced-q()
{
    let $keywords := fn:tokenize(xdmp:get-request-field("keywords")," ")
    let $type := xdmp:get-request-field("type")
    let $exclude := fn:tokenize(xdmp:get-request-field("exclude")," ")  
    let $genre := xdmp:get-request-field("genre")    
    let $genre := if ($genre eq "all")
                  then ""
                  else $genre
    let $creator := xdmp:get-request-field("creator")
    let $songtitle := xdmp:get-request-field("songtitle")
    
    let $keywords := 
      if($keywords) 
      then 
        if($type eq "any")
        then fn:string-join($keywords," OR ")
        else if($type eq "phrase")
             then fn:concat('"',fn:string-join($keywords," "),'"')
             else $keywords
      else ()

    let $exclude := 
        if($exclude)
        then fn:string-join((for $i in $exclude 
                             return fn:concat("-",$i))," ")
        else ()
        
    let $genre :=
        if($genre)
        then
            if (fn:matches($genre,"\W"))
            then fn:concat('genre:"',$genre,'"')
            else fn:concat("genre:",$genre)
        else () 
        
    let $creator :=
        if($creator)
        then
            if (fn:matches($creator,"\W"))
            then fn:concat('creator:"',$creator,'"')
            else fn:concat("creator:",$creator)
        else ()         

    let $songtitle :=
        if($songtitle)
        then
            if (fn:matches($songtitle,"\W"))
            then fn:concat('title:"',$songtitle,'"')
            else fn:concat("title:",$songtitle)
        else ()
         
    let $q-text := fn:string-join(($keywords,$exclude,$genre,$creator,$songtitle)," ")
    return $q-text
};