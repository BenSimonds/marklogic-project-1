module namespace disp = "http://marklogic.com/MLU/top-songs/display";
declare namespace ts="http://marklogic.com/MLU/top-songs";
import module namespace search = "http://marklogic.com/appservices/search" at "/MarkLogic/appservices/search/search.xqy";

declare function description($song)
{
    for $text in $song/search:snippet/search:match/node() 
    return
		if(fn:node-name($text) eq xs:QName("search:highlight"))
		then <span class="highlight">{$text/text()}</span>
		else $text
};

declare function display-song-detail($uri)
{
	let $song := fn:doc($uri) 
	return <div>
		<div class="songnamelarge">"{$song/ts:top-song/ts:title/text()}"</div>
		{if ($song/ts:top-song/ts:album/@uri) then <div class="albumimage"><img src="get-file.xqy?uri={xdmp:url-encode($song/ts:top-song/ts:album/@uri)}"/></div> else ()}
		<div class="detailitem">#1 weeks: {fn:count($song/ts:top-song/ts:weeks/ts:week)}</div>	
		<div class="detailitem">weeks: {fn:string-join(($song/ts:top-song/ts:weeks/ts:week), ", ")}</div>	
		{if ($song/ts:top-song/ts:genres/ts:genre) then <div class="detailitem">genre: {fn:lower-case(fn:string-join(($song/ts:top-song/ts:genres/ts:genre), ", "))}</div> else ()}
		{if ($song/ts:top-song/ts:artist/text()) then <div class="detailitem">artist: {$song/ts:top-song/ts:artist/text()}</div> else ()}
		{if ($song/ts:top-song/ts:album/text()) then <div class="detailitem">album: {$song/ts:top-song/ts:album/text()}</div> else ()}
		{if ($song/ts:top-song/ts:writers/ts:writer) then <div class="detailitem">writers: {fn:string-join(($song/ts:top-song/ts:writers/ts:writer), ", ")}</div> else ()}
		{if ($song/ts:top-song/ts:producers/ts:producer) then <div class="detailitem">producers: {fn:string-join(($song/ts:top-song/ts:producers/ts:producer), ", ")}</div> else ()}
		{if ($song/ts:top-song/ts:label) then <div class="detailitem">label: {$song/ts:top-song/ts:label}</div> else ()}
		{if ($song/ts:top-song/ts:formats/ts:format) then <div class="detailitem">formats: {fn:string-join(($song/ts:top-song/ts:formats/ts:format), ", ")}</div> else ()} 
		{if ($song/ts:top-song/ts:lengths/ts:length) then <div class="detailitem">lengths: {fn:string-join(($song/ts:top-song/ts:lengths/ts:length), ", ")}</div> else ()}
		{if ($song/ts:top-song/ts:descr) then <div class="detailitem">{$song/ts:top-song/ts:descr}</div> else ()}
		</div>
};