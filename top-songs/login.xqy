xquery version "1.0-ml";
let $username := xdmp:get-request-field("username") 
let $password := xdmp:get-request-field("password") 
return xdmp:login($username, $password), 
xdmp:redirect-response("index.xqy")